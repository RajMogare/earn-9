import logo from './logo.svg';
import './App.css';
import useFetch from 'react-fetch-hook'

function App() {

  const {data:posts,isLoading,error}=useFetch('https://jsonplaceholder.typicode.com/posts')
  if(isLoading){
    return <h2>Loading...</h2>
  }
  if(error){
    return <h2>Error: error fetching</h2>
  }

  return (
    <div className="App">
        <h1>Post Of Users</h1>
        {posts.map((post)=>(
          <div key={post.id}>
            <h2>{post.title}</h2>
            {post.body}
          </div>
        ))}
    </div>
  );
}

export default App;
